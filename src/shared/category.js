import React from 'react'; 
import Product from './product';

const Category = ({ categories , addtocart}) => {
	// console.log(props)
	
	const catList = categories.map(cat => {
		return (
			<div className="col-12" key={cat.id}>
				<div className="card bg-secondary catCard nbr-card">
					<div className="card-header">
						<label>{cat.name} --&nbsp;</label>{cat.description}
					</div>
					<div className="card-body bg-dark ">
						<div className="row">
							<Product products={cat.products} addtocart={addtocart} />
						</div>
					</div>
				</div>
				
			</div>
		)
	});
	return catList
}

export default Category
