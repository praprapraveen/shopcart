import React, { Component } from 'react';

class Modal extends Component {
	render() {
		const { content } = this.props;
		return(
			<div id="Modal" className="modal fade" role="dialog">
				<div className="modal-dialog modal-sm">
					<div className="modal-content">
						{ content }
					</div>
				</div>
			</div>
		)
	}
	

}

export default Modal