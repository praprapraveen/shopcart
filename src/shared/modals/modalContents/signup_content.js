import React, { Component } from 'react'; 
import '../../shared.css';

class SignupContent extends Component {
	state = {
		name : null,
		email : null,
		username : null,
		password : null,
		cpassword : null
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id] : e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault();
		document.getElementById('signup').reset();
		this.props.signup(this.state);
		
	}
	render() {
		return (
			<div>
				<div className="modal-header">
					<div className="float-left">Signup</div>
					<button type="button" className="close" data-dismiss="modal">{'\u00d7'}</button>
				</div>
				<div className="modal-body">
					<form id='signup'>
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="name" className="form-label">Name:</label>
		    						<input type="text" className="form-control" id="name" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="email" className="form-label">Email:</label>
		    						<input type="text" className="form-control" id="email" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="username" className="form-label">Username:</label>
		    						<input type="text" className="form-control" id="username" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="password" className="form-label">Password:</label>
		    						<input type="password" className="form-control" id="password" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="cpassword" className="form-label">Confirm Password:</label>
		    						<input type="password" className="form-control" id="cpassword" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group" align="center">
									<button type="submit" data-dismiss="modal" className="btn btn-sm btn-outline-primary" onClick={this.handleSubmit} >Signup</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}
export default SignupContent