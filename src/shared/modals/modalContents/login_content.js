import React,{ Component } from 'react'; 


class LoginContent extends Component {
	state = {
		username: null,
		password: null,
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id] : e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault();
		document.getElementById('login').reset();
		this.props.login(this.state);
		
	}
	render() {
		return (
			<div>
				<div className="modal-header">
					<div className="float-left">Login</div>
					<button type="button" className="close" data-dismiss="modal">{'\u00d7'}</button>
				</div>
				<div className="modal-body">
					<form id="login">
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="username" className="form-label">Username:</label>
		    						<input type="text" className="form-control" id="username" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="password" className="form-label">Password:</label>
		    						<input type="password" className="form-control" id="password" onChange={this.handleChange} />
								</div>
							</div>
							<div className="col-12">
								<div className="form-group" align="center">
									<button type="submit" data-dismiss="modal" className="btn btn-sm btn-outline-primary" onClick={this.handleSubmit} >Login</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		)
	}
}

export default LoginContent