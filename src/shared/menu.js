import React, { Component } from 'react'; 
import { Link } from 'react-router-dom'

class Menu extends Component {
	constructor() {
		super()
		this.loginbtn = React.createRef();
		this.signupbtn = React.createRef();
	}
	handleModalContent = (e) => {
		let refView = e.target.getAttribute('data-content');
		this.props.modalcontentstate(refView);
	}
	handleLogout = (e) => {
		this.props.logout();
	}
	simulate = (reference) => {
		if(reference == 'login') { this.loginbtn.current.click(); }
		else if(reference == 'signup') { this.signupbtn.current.click(); }
	}
	render() {
		const { session } = this.props;
		return (
			<div className="col-12">
				<div className="card bg-dark catCard nbr-card">
					<div className="card-header">
						<div className="btn-group float-left">
							<Link to="/" className="btn btn-sm btn-outline-success " >Home</Link>
						</div>	
						{!session.loggedInStat ? (
							<div className="btn-group float-right">
								<button ref={this.loginbtn} className="btn btn-sm btn-outline-success " data-toggle="modal" data-target="#Modal" data-content="login" onClick={this.handleModalContent} >Login</button>
								<button ref={this.signupbtn} className="btn btn-sm btn-outline-success " data-toggle="modal" data-target="#Modal" data-content="signup" onClick={this.handleModalContent} >Sign up</button>
							</div>
						) : (
							<div className="btn-group float-right">
								<Link to="/cart" className="btn btn-sm btn-outline-success " >View Cart</Link>
								<button className="btn btn-sm btn-outline-success" onClick={this.handleLogout} >Logout</button>
							</div>
						)}
						
					</div>
				</div>
				
			</div>
		)
	}	
	
	
}

export default Menu