import React, { Component } from 'react'; 
import { Link } from 'react-router-dom';

class Product extends Component {

	handleaddtocart = (e) => {
		let id = e.target.getAttribute('data-prd');
		this.props.addtocart(id)
	}

	render(){
		const { products } = this.props;
		const prodList = products.map(prd => {
			return (
				<div className="col-3" key={prd.id}>
					<div className="card prdCard ">
						<div className="card-header">
							{prd.name}
						</div>
						<div className="card-body">
							<img src={prd.image} alt="Product" className="img-fluid mx-auto d-block prdImage" />
							<p>{prd.description}</p>
						</div>
						<div className="card-footer">
							<button className="btn btn-sm btn-outline-info float-left" data-prd={prd.id} onClick={this.handleaddtocart} >Add to cart</button>
							<Link to={`/view_products/${prd.id}`} className="btn btn-sm btn-outline-secondary float-right">View Product</Link>
						</div>
					</div>
				</div>
			)
		});
		return prodList
	}
	
}

export default Product;