import React, { Component } from 'react';
import{ Link } from 'react-router-dom';


class ViewProduct extends Component {
	handleaddtocart = (e) => {
		let id = e.target.getAttribute('data-prd');
		this.props.addtocart(id)
	}
	render() {
		const { match } = this.props;
		const { data } = this.props;
		let selectedProd;
		for(let c= 0; c<data.length; c++) {
			for(let i = 0; i<data[c].products.length; i++) {
				if(match.params.id == data[c].products[i].id) {
					selectedProd = data[c].products[i];	
				}
			}
		}
		return (
			<div className="col-12" >
				<div className="card">
					<div className="card-header">
						{ selectedProd.name }
					</div>
					<div className="card-body">
						<img src={`${selectedProd.image}`} alt="Product" className="img-fluid mx-auto d-block prdImage" />
						<p>{ selectedProd.description }</p>
					</div>
					<div className="card-footer align-items-center">
						<Link to="/" className="btn btn-sm btn-outline-info float-left">Back</Link>
						<button className="btn btn-sm btn-outline-info float-right" data-prd={selectedProd.id} onClick={this.handleaddtocart} >Add to cart</button>

						
					</div>
				</div>
			</div>
		)
		
		
	}
}

export default ViewProduct