import React, { Component } from 'react'; 
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import './App.css';
import Home from './home/home';
import ViewProduct from './viewProducts/viewProducts';
import ViewCart from './viewcart/viewcart';

import Menu from './shared/menu';
import Modal from './shared/modals/modal';
import LoginContent from './shared/modals/modalContents/login_content';
import SignupContent from './shared/modals/modalContents/signup_content';

import './shared/shared.css';

class App extends Component {

  constructor(props) {
    super(props)
    console.log(props)
    this.modalref = React.createRef();
    this.addToCart = this.addToCart.bind(this);
    this.postData = this.postData.bind(this);
  }

  state = {
      category: [
        {
          name: 'Bags', description: 'Travel bags, Office bags, Suitcases', id: 1, 
          products: [
            {name: 'Tourister', description: 'Travel Bags', catId: 1, image: './assets/images/bags/bag1.jpg', id: 1, rate: 850, instock:40 },
            {name: 'Twin', description: 'Travel Bags', catId: 1, image: './assets/images/bags/sbag1.jpg', id: 2, rate: 1050, instock:140 },
            {name: 'Proffice', description: 'Travel Bags', catId: 1, image: './assets/images/bags/subag1.jpeg', id: 3, rate: 999, instock:200 },
          ]
        },
        { 
          name: 'Shoes', description: 'Sports, Casual, Executive', id: 2,
          products: [
            {name: 'Reebok', description: 'Sports', catId: 2, image: './assets/images/shoes/shu1.jpg', id: 4, rate: 1250, instock:340 },
            {name: 'Tria', description: 'Executive', catId: 2, image: './assets/images/shoes/shu2.jpg', id: 5, rate: 1550, instock:100 },
            {name: 'Didass', description: 'Travel Bags', catId: 2, image: './assets/images/shoes/shu3.jpg', id: 6, rate: 1700, instock:400 }
          ]
        },
        { 
          name: 'Shirts', description: 'Regular, Casual, Executive', id: 3,
          products: [
            {name: 'Peter England', description: 'Regular', catId: 3, image: './assets/images/shirts/shirt1.jpg', id: 7, rate: 1250, instock:100 },
            {name: 'Double Bull', description: 'Executive', catId: 3, image: './assets/images/shirts/shirt2.jpeg', id: 8, rate: 2250, instock:40 },
            {name: 'Shriek', description: 'Casual', catId: 3, image: './assets/images/shirts/shirt3.jpg', id: 9, rate: 940, instock:300 },
          ]
        },
        { 
          name: 'Wallets', description: 'Travel Wallets, Pocket Wallets', id: 4,
          products: [
            {name: 'Zuka', description: 'Pocket Wallets', catId: 4, image: './assets/images/wallets/wlt1.jpg', id: 10, rate: 1000, instock:40 },
            {name: 'Alliga', description: 'Travel Wallets', catId: 4, image: './assets/images/wallets/wlt2.jpg', id: 11, rate: 1250, instock:75 },
            {name: 'Patra', description: 'Pocket Wallets', catId: 4, image: './assets/images/wallets/wlt3.jpg', id: 12, rate: 250, instock:40 },
          ]
        }
      ],
      users : [
        {
          id: 1, name: 'Tintu Mon', email: 'tintumon@tintumail.com', username: 'tintu007', password: 'tintubintu'
        },
        {
          id: 2, name: 'Praveen', email: 'praveen@gmail.com', username: 'prapra', password: '123123'
        }
      ],
      cart : [
        {
          id: 1, prd_id: 10, prd_orderRate: 1000, prd_orderQty: 1, prd_orderedBy: 1
        }
      ],
      pendingcart : {},
      session : {
        loggedInStat: false, user: null, username: null, email: null, authToken: null
      },
      modalcontentstate : null
  }

  login = (data) => {
    let logedinuser = '';
    for (var i = this.state.users.length - 1; i >= 0; i--) {
      if(this.state.users[i].username == data.username && this.state.users[i].password == data.password) {
        logedinuser = this.state.users[i];
      }
    }
    if(logedinuser != '') {
      let newsession = {
        loggedInStat : true,
        user : logedinuser.id,
        username : logedinuser.username,
        email : logedinuser.email,
        authToken : Math.random()
      }
      this.setState({
        session: newsession
      }, () => {
        if(Object.keys(this.state.pendingcart).length > 0){
          this.addToCart(this.state.pendingcart.prd_id)
        }
      });
    } else {

    }
  }

  signup = (data) => {
    // data['id']= Math.random();
    let sanitisedData = {};
    let frm= new FormData();
    for (const key of Object.keys(data)) {
      if(key != 'cpassword') { 
        sanitisedData[key] = data[key]; 
        frm.append(key, data[key]);
      }
    }

    this.postData(`http://localhost:3001/signup`, JSON.stringify(sanitisedData))
    .then((data) => console.log(data)) // JSON-string from `response.json()` call
    .catch((error) => console.error(error));

  
    
    let newUsers = [...this.state.users, sanitisedData];
    this.setState({
      users : newUsers
    },() => {
      console.log(this.state.users)
    });
  }

  logout = () => {
    let resetSession = {
      loggedInStat: false,
      user: null,
      username: null,
      email: null,
      authToken: null
    }
        
    this.setState({
      session : resetSession
    }, () => {
      this.props.history.push("/")
    });
  }

  addToCart = (id) => {
    let logedinuser = this.state.session;
    let newitem = {};
    let selprd;
    for(let c= 0; c<this.state.category.length; c++) {
      for(let i = 0; i<this.state.category[c].products.length; i++) {
        if(id == this.state.category[c].products[i].id) {
          selprd = this.state.category[c].products[i]; 
        }
      }
    }
    let newpndcrt = {
      id : Math.random(),
      prd_id: selprd.id, 
      prd_orderRate: selprd.rate, 
      prd_orderQty: 1, 
      prd_orderedBy: logedinuser.user
    }
    if(logedinuser.loggedInStat) {
      let newcart = [...this.state.cart, newpndcrt];
      this.setState({
        cart : newcart,
        pendingcart : {}
      }, () => { console.log(this.state.cart) });
    } else {
      
      this.setState({
        pendingcart : newpndcrt
      })
      this.modalref.current.simulate('login');
    }
  }

  removeFromCart = (id) => {
    let filteredcart = this.state.cart.filter(cartPrd => {
        return cartPrd.id !== id;
      });
    this.setState({
      cart: filteredcart
    }, () => { console.log(this.state.cart)})
  }

  

  handleModalContentState = (contentState) => {
    let content;
    if( contentState == 'login') { content = <LoginContent login={this.login} />; }
    if( contentState == 'signup') { content = <SignupContent signup={this.signup} />; }
    this.setState({modalcontentstate : content});
  }

  componentDidMount = () => {
    this.getData('http://localhost:3001/getusers')
    .then((response) => response.json())
    .then((body) => {
      let nusr = this.state.users;
      body.map(dt => {
        nusr = [...nusr, dt];
      })
      this.setState({
        users : nusr
      },() => {
        console.log(this.state.users)
      });
    })
  }

  postData = (url, data) => {
    return fetch(url, {
      method: "POST",
      mode: "cors", 
      headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin" : '*'
      },
     body: data,
    })
    .then((response) => response);
  }

  getData = (url) => {
    return fetch(url, {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin" : '*'
      }
    })
  }
  
  
  render() {

    return (
      <div className="App">
        <div className="container">
        <div className="row">
            <Menu ref={this.modalref} modalcontentstate={this.handleModalContentState} session={this.state.session} logout={this.logout} />
            <Switch>
              <Route exact path="/" render={ (props)=><Home {...props} data={ this.state.category } addtocart={this.addToCart} /> } />
              <Route path="/view_products/:id" render={ (props)=><ViewProduct {...props} data={ this.state.category } addtocart={this.addToCart} /> } />
              <Route path="/cart" render={ ()=><ViewCart data={ this.state.cart} session={this.state.session} category={this.state.category} remove={this.removeFromCart}  /> } />
              <Redirect to="/" />
            </Switch>
            <Modal  content={this.state.modalcontentstate} />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(App);
