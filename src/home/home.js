import React, { Component } from 'react'; 
import Category from '../shared/category';


class Home extends Component {
	
	

	render() {
		const { data } = this.props;
		return (
			<Category addtocart={this.props.addtocart} categories={data} />
		)
		
		
	}
}

export default Home;