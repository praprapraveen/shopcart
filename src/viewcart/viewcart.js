import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// id: 1, prd_id: 10, prd_orderRate: 1000, prd_orderQty: 1, prd_orderedBy: 1
// loggedInStat: false, user: null, username: null, email: null, authToken: null
// {name: 'Reebok', description: 'Sports', catId: 2, image: './assets/images/shoes/shu1.jpg', id: 4, rate: 1250, instock:340 },
class ViewCart extends Component {
	
	render() {
		const { data, session, category } = this.props;
		let selprd, selcat;
		let prodlist;
		if(session.loggedInStat) {
			let filteredmycart = data.filter(cartPrd => {
				if(cartPrd.prd_orderedBy === session.user) {
					return true;
				}
			});
			prodlist = filteredmycart.map(prd => {
				for(let c= 0; c<category.length; c++) {
			      for(let i = 0; i<category[c].products.length; i++) {
			        if(prd.prd_id == category[c].products[i].id) {
			          selprd = category[c].products[i]; 
			          selcat = category[c]
			        }
			      }
			    }
				return (
					<div className="row carthead" key={prd.id}>
						<div className="col-3 pad15">
							<img src={selprd.image} alt="Product" className="img-fluid mx-auto d-block prdImagecart" />
						</div>
						<div className="col-6 pad15">
							<div className="col-12">
								<label>Product Name: </label> {selprd.name}
							</div>
							<div className="col-12">
								<label>Product Category: </label> {selcat.name}
							</div>
							<div className="col-12">
								<label>Order Quantity: </label> {prd.prd_orderQty}
							</div>
							<div className="col-12">
								<label>Order Price: </label> {prd.prd_orderRate}
							</div>
						</div>
						<div className="col-3 pad15 ">
							<div className="btn-group-vertical float-right">
								<Link to={`/view_products/${prd.prd_id}`} className="btn btn-sm btn-success" >View Product</Link>
								<button className="btn btn-sm btn-info" data-cartid={prd.id} onClick={() => {this.props.remove(prd.id)}} >Remove from cart</button>
							</div>
						</div>
						
					</div>
				)
			})
		}
		return (
			<div className="col-12">
				
				{ prodlist }
			</div>
		)
	}
}

export default ViewCart